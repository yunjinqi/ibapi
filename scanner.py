"""
Copyright (C) 2019 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable.
"""


from ibapi.object_implem import Object
from ibapi.common import UNSET_INTEGER, UNSET_DOUBLE


class ScanData(Object):
    def __init__(self, contract = None, rank = 0, distance = "", benchmark = "", projection = "", legsStr = ""):
        # 合约
        self.contract = contract
        # 排名
        self.rank = rank
        # distance\benchmark\projection\legsStr跟请求的品种有关，有可能是空的
        self.distance = distance
        # 基准
        self.benchmark = benchmark
        # projection
        self.projection = projection
        # legsStr
        self.legsStr = legsStr

    def __str__(self):
        return "Rank: %d, Symbol: %s, SecType: %s, Currency: %s, Distance: %s, Benchmark: %s, Projection: %s, Legs String: %s" % (self.rank, 
            self.contract.symbol, self.contract.secType, self.contract.currency, self.distance,
            self.benchmark, self.projection, self.legsStr)


NO_ROW_NUMBER_SPECIFIED = -1


class ScannerSubscription(Object):
    def __init__(self):
        # 请求返回的行数
        self.numberOfRows = NO_ROW_NUMBER_SPECIFIED
        # 获取contract的种类，比如股票、期货等，参考值如下：
            # STK
            # STOCK_EU
            # FUT_EU
            # IND_EU
            # SLB_US
            # FUT_HK
            # IND_HK
            # WAR_EU
            # STOCK_HK
            # FUT_NA
            # IND_US
            # PMONITOR
            # STOCK_NA
            # FUT_US
            # EFP
            # PMONITORM
            # BOND
        self.instrument = ""
        # 请求哪个交易所的合约
            # STK.NASDAQ
            # STK.EU.IBIS-ETF
            # FUT.HK.HKFE
            # FUT.EU.FTA
            # STK.EU.SBVM
            # FUT.HK.JAPAN
            # FUT.EU.IDEM
            # STK.NYSE
            # STK.EU.IBIS-NEWX
            # FUT.HK.KSE
            # FUT.EU.LIFFE
            # STK.AMEX
            # STK.EU.IBIS-EUSTARS
            # FUT.NYSELIFFE
            # FUT.EU.MEFFRV
            # STK.ARCA
            # STK.EU.IBIS-XETRA
            # FUT.HK.OSE.JPN
            # IND.HK.OSE.JPN
            # STK.NASDAQ.NMS
            # STK.EU.LSE
            # FUT.HK.SGX
            # FUT.EU.BELFOX
            # STK.NASDAQ.SCM
            # STK.EU.SBF
            # FUT.HK.SNFE
            # IND.US
            # STK.US.MAJOR
            # STK.EU.IBIS-USSTARS
            # FUT.HK.TSE.JPN
            # IND.HK.TSE.JPN
            # STK.US.MINOR
            # STK.EU.SFB
            # FUT.HK
            # IND.EU.DTB
            # STK.OTCBB
            # STK.EU.SWISS

        self.locationCode = ""
        # 请求的代码
            # TOP_PERC_GAIN
            # TOP_PERC_LOSE
            # HIGH_VS_52W_HL
            # LOW_VS_13W_HL
            # MOST_ACTIVE
            # HIGH_VS_13W_HL
            # LOW_VS_26W_HL
            # ALL_SYMBOLS_ASC
            # HIGH_VS_26W_HL
            # LOW_VS_52W_HL
            # HIGH_BOND_ASK_CURRENT_YIELD_ALL
            # HIGH_SYNTH_BID_REV_NAT_YIELD
            # LOW_WAR_REL_IMP_VOLAT
            # BOND_CUSIP_AZ
            # HOT_BY_OPT_VOLUME
            # MARKET_CAP_USD_ASC
            # BOND_CUSIP_ZA
            # HOT_BY_PRICE
            # MARKET_CAP_USD_DESC
            # FAR_MATURITY_DATE
            # HOT_BY_PRICE_RANGE
            # MOST_ACTIVE_AVG_USD
            # HALTED
            # HOT_BY_VOLUME
            # MOST_ACTIVE_USD
            # ALL_SYMBOLS_DESC
            # LIMIT_UP_DOWN
            # NEAR_MATURITY_DATE
            # HIGH_BOND_ASK_YIELD_ALL
            # LOW_BOND_BID_CURRENT_YIELD_ALL
            # NOT_OPEN
            # HIGH_BOND_DEBT_2_BOOK_RATIO
            # LOW_BOND_BID_YIELD_ALL
            # OPT_OPEN_INTEREST_MOST_ACTIVE
            # HIGH_BOND_DEBT_2_EQUITY_RATIO
            # LOW_BOND_DEBT_2_BOOK_RATIO
            # OPT_VOLUME_MOST_ACTIVE
            # HIGH_BOND_DEBT_2_TAN_BOOK_RATIO
            # LOW_BOND_DEBT_2_EQUITY_RATIO
            # PMONITOR_AVAIL_CONTRACTS
            # HIGH_BOND_EQUITY_2_BOOK_RATIO
            # LOW_BOND_DEBT_2_TAN_BOOK_RATIO
            # PMONITOR_CTT
            # HIGH_BOND_EQUITY_2_TAN_BOOK_RATIO
            # LOW_BOND_EQUITY_2_BOOK_RATIO
            # PMONITOR_IBOND
            # HIGH_BOND_NET_ASK_CURRENT_YIELD_ALL
            # LOW_BOND_EQUITY_2_TAN_BOOK_RATIO
            # PMONITOR_RFQ
            # HIGH_BOND_NET_ASK_YIELD_ALL
            # LOW_BOND_NET_BID_CURRENT_YIELD_ALL
            # TOP_STOCK_BUY_IMBALANCE_ADV_RATIO
            # HIGH_BOND_NET_SPREAD_ALL
            # LOW_BOND_NET_BID_YIELD_ALL
            # TOP_OPT_IMP_VOLAT_LOSE
            # HIGH_MOODY_RATING_ALL
            # LOW_BOND_NET_SPREAD_ALL
            # TOP_OPT_IMP_VOLAT_GAIN
            # HIGH_COUPON_RATE
            # LOW_BOND_SPREAD_ALL
            # TOP_OPEN_PERC_LOSE
            # HIGH_DIVIDEND_YIELD
            # LOW_COUPON_RATE
            # TOP_PRICE_RANGE
            # HIGH_DIVIDEND_YIELD_IB
            # LOWEST_SLB_ASK
            # TOP_STOCK_SELL_IMBALANCE_ADV_RATIO
            # HIGHEST_SLB_BID
            # LOW_GROWTH_RATE
            # TOP_OPEN_PERC_GAIN
            # HIGH_GROWTH_RATE
            # LOW_MOODY_RATING_ALL
            # TOP_TRADE_COUNT
            # HIGH_BOND_SPREAD_ALL
            # LOW_OPEN_GAP
            # TOP_TRADE_RATE
            # HIGH_OPEN_GAP
            # LOW_OPT_IMP_VOLAT
            # TOP_VOLUME_RATE
            # HIGH_OPT_IMP_VOLAT
            # LOW_OPT_IMP_VOLAT_OVER_HIST
            # WSH_NEXT_ANALYST_MEETING
            # HIGH_OPT_OPEN_INTEREST_PUT_CALL_RATIO
            # LOW_OPT_OPEN_INTEREST_PUT_CALL_RATIO
            # WSH_NEXT_EARNINGS
            # HIGH_OPT_IMP_VOLAT_OVER_HIST
            # LOW_OPT_VOLUME_PUT_CALL_RATIO
            # WSH_NEXT_EVENT
            # HIGH_PE_RATIO
            # LOW_PE_RATIO
            # WSH_NEXT_MAJOR_EVENT
            # HIGH_OPT_VOLUME_PUT_CALL_RATIO
            # LOW_PRICE_2_BOOK_RATIO
            # WSH_PREV_ANALYST_MEETING
            # HIGH_PRICE_2_BOOK_RATIO
            # LOW_PRICE_2_TAN_BOOK_RATIO
            # WSH_PREV_EARNINGS
            # HIGH_PRICE_2_TAN_BOOK_RATIO
            # LOW_QUICK_RATIO
            # WSH_PREV_EVENT
            # HIGH_QUICK_RATIO
            # LOW_RETURN_ON_EQUITY
        self.scanCode =  ""
        # 设置该价格之后，价格在该价格下面的contract将会被过滤掉
        self.abovePrice = UNSET_DOUBLE
        # 设置该价格之后，价格在该价格上面的contract将会被过滤掉
        self.belowPrice = UNSET_DOUBLE
        # 设置该成交量之后，成交量在该值下面的contract将会被过滤掉
        self.aboveVolume = UNSET_INTEGER
        # 设置之后，市值小于该值将会被过滤掉
        self.marketCapAbove = UNSET_DOUBLE
        # 设置之后，市值高于该值得合约将会被过滤掉
        self.marketCapBelow = UNSET_DOUBLE
        # 设置之后，穆迪评级低于该值得将会被过滤掉
        self.moodyRatingAbove =  ""
        # 设置之后，穆迪评级高于该值得将会被过滤掉
        self.moodyRatingBelow =  ""
        # 设置之后，标普评级低于该值得将会被过滤掉
        self.spRatingAbove =  ""
        # 设置之后，标普评级高于该值得将会被过滤掉
        self.spRatingBelow =  ""
        # 设置之后，到期日早于该值得将会被过滤掉
        self.maturityDateAbove =  ""
        # 设置之后，到期日晚于该值得将会被过滤掉
        self.maturityDateBelow =  ""
        # 设置之后，息票率低于该值得将会被过滤掉
        self.couponRateAbove = UNSET_DOUBLE
        # 设置之后，息票率高于该值得将会被过滤掉
        self.couponRateBelow = UNSET_DOUBLE 
        # 设置之后，将会过滤掉可转换债券
        self.excludeConvertible = False
        # 设置该值之后，其期权的成交量小于该值，将会被过滤掉
        self.averageOptionVolumeAbove = UNSET_INTEGER
        # 给扫描仪提供 pair value
        self.scannerSettingPairs =  ""
        # 根据股票类型进行过滤
        self.stockTypeFilter =  ""

    def __str__(self):
        s = "Instrument: %s, LocationCode: %s, ScanCode: %s" % (self.instrument, self.locationCode, self.scanCode)
        return s

