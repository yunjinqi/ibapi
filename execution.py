"""
Copyright (C) 2019 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable.
"""


from ibapi.object_implem import Object
from ibapi.common import UNSET_DECIMAL
from ibapi.utils import decimalMaxString


class Execution(Object):
    def __init__(self):
        # 订单执行的id
        self.execId = ""
        # 订单执行的时间
        self.time =  ""
        # 账户中分配的数字
        self.acctNumber =  ""
        # 交易所
        self.exchange =  ""
        # 订单的方向
        self.side = ""
        # 成交的数量
        self.shares = UNSET_DECIMAL
        # 成交的价格(不含佣金)
        self.price = 0. 
        # TWS给order设定的id
        self.permId = 0
        # 提交订单的客户端id
        self.clientId = 0
        # 订单id
        self.orderId = 0
        # IB清算时订单是否执行
        self.liquidation = 0
        # 累计的量
        self.cumQty = UNSET_DECIMAL
        # 订单成交的平均价格
        self.avgPrice = 0.
        # 用户给订单自定义的字符串
        self.orderRef =  ""
        # 不理解这个参数的意义(Economic value Rule name and optional argument)
        self.evRule =  ""
        # 合约价格变化1市场价格变化的倍数
        self.evMultiplier = 0.
        # mode code
        self.modelCode =  ""
        # Liquidity type of the execution
        self.lastLiquidity = 0

    def __str__(self):
        return "ExecId: %s, Time: %s, Account: %s, Exchange: %s, Side: %s, Shares: %s, Price: %f, PermId: %d, " \
                "ClientId: %d, OrderId: %d, Liquidation: %d, CumQty: %s, AvgPrice: %f, OrderRef: %s, EvRule: %s, " \
                "EvMultiplier: %f, ModelCode: %s, LastLiquidity: %d" % (self.execId, self.time, self.acctNumber, 
                self.exchange, self.side, decimalMaxString(self.shares), self.price, self.permId, self.clientId, self.orderId, self.liquidation,
                decimalMaxString(self.cumQty), self.avgPrice, self.orderRef, self.evRule, self.evMultiplier, self.modelCode, self.lastLiquidity)


class ExecutionFilter(Object):
    # Filter fields
    def __init__(self):
        # clientId
        self.clientId = 0
        # 账户代码
        self.acctCode = ""
        # 时间
        self.time = ""
        # 品种名称
        self.symbol = ""
        # 交易品种种类
        self.secType = ""
        # 交易所
        self.exchange = "" 
        # 方向
        self.side = ""

