"""
Copyright (C) 2019 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable.
"""

from ibapi.object_implem import Object 
from ibapi import utils


class CommissionReport(Object):
    def __init__(self):
        # 产生佣金的订单执行的id
        self.execId = ""
        # 佣金
        self.commission = 0. 
        # 佣金的货币
        self.currency = ""
        # 实现的盈亏
        self.realizedPNL =  0.
        # 订单产生的收入
        self.yield_ = 0.
        # 收入产生的日期
        self.yieldRedemptionDate = 0  # YYYYMMDD format

    def __str__(self):
        return "ExecId: %s, Commission: %f, Currency: %s, RealizedPnL: %s, Yield: %s, YieldRedemptionDate: %d" % (self.execId, self.commission, 
            self.currency, utils.floatToStr(self.realizedPNL), utils.floatToStr(self.yield_), self.yieldRedemptionDate)
