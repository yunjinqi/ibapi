"""
Copyright (C) 2019 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable.
"""

from ibapi.common import UNSET_DOUBLE


class OrderState:
    def __init__(self):
        # 现在订单的状态
        self.status= ""
        # 下单前的初始保证金
        self.initMarginBefore= ""
        # 下单前的维持保证金
        self.maintMarginBefore= ""
        # 下单前包含贷款的账户权益
        self.equityWithLoanBefore= ""
        # 初始保证金的变化
        self.initMarginChange= ""
        # 维持保证金的变化
        self.maintMarginChange= ""
        # 包含贷款的账户权益的变化
        self.equityWithLoanChange= ""
        # 下单后的初始保证金
        self.initMarginAfter= ""
        # 下单后的维持保证金
        self.maintMarginAfter= ""
        # 下单后包含贷款的账户权益的变化
        self.equityWithLoanAfter= ""
        # 订单产生的佣金
        self.commission = UNSET_DOUBLE      # type: float
        # 订单执行最少需要的佣金
        self.minCommission = UNSET_DOUBLE   # type: float
        # 订单执行最多需要的佣金
        self.maxCommission = UNSET_DOUBLE   # type: float
        # 订单佣金的货币
        self.commissionCurrency = ""
        # 警告信息
        self.warningText = ""
        # 订单完成时间
        self.completedTime = ""
        # 订单完成状态
        self.completedStatus = ""
