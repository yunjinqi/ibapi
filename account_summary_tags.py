"""
Copyright (C) 2019 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable.
"""


class AccountSummaryTags:
    # IB的账户类型
    AccountType = "AccountType"
    # 按照字面意思是净的清算价值
    NetLiquidation = "NetLiquidation"
    # 总的现金价值
    TotalCashValue = "TotalCashValue"
    # 结算时的现金
    SettledCash = "SettledCash"
    # 应计现金
    AccruedCash = "AccruedCash"
    # 购买力
    BuyingPower = "BuyingPower"
    # 算上贷款的权益
    EquityWithLoanValue = "EquityWithLoanValue"
    # 前一个交易日4:00 PM EST的带贷款的账户价值
    PreviousEquityWithLoanValue = "PreviousEquityWithLoanValue"
    # 总的持仓将至
    GrossPositionValue = "GrossPositionValue"
    # Regulation T equity for universal account
    ReqTEquity = "ReqTEquity"
    # Regulation T margin for universal account
    ReqTMargin = "ReqTMargin"
    # Special Memorandum Account - line of credit created when Reg T securities increase in value
    SMA = "SMA"
    # 初始的保证金需求
    InitMarginReq = "InitMarginReq"
    # 维持保证金需求
    MaintMarginReq = "MaintMarginReq"
    # 可以使用的资金
    AvailableFunds = "AvailableFunds"
    #  超额流动性
    ExcessLiquidity = "ExcessLiquidity"
    # 保证金缓冲
    Cushion = "Cushion"
    # 没有打折或者日内信用的初始保证金
    FullInitMarginReq = "FullInitMarginReq"
    # 没有打折或者日内信用的维持保证金
    FullMaintMarginReq = "FullMaintMarginReq"
    # 没有打折或者日内信用的可用资金
    FullAvailableFunds = "FullAvailableFunds"
    # 没有打折或者日内信用的超额流动性
    FullExcessLiquidity = "FullExcessLiquidity"
    # 保证金下次改变的时间
    LookAheadNextChange = "LookAheadNextChange"
    # 保证金改变之后需要的初始保证金
    LookAheadInitMarginReq = "LookAheadInitMarginReq"
    # 保证金改变之后需要的维持保证金
    LookAheadMaintMarginReq = "LookAheadMaintMarginReq"
    # 保证金改变之后的可用资金
    LookAheadAvailableFunds = "LookAheadAvailableFunds"
    # 保证金改变之后超额流动性
    LookAheadExcessLiquidity = "LookAheadExcessLiquidity"
    # 爆仓风险
    HighestSeverity = "HighestSeverity"
    # 当日交易还剩多少
    DayTradesRemaining = "DayTradesRemaining"
    # 杠杆
    Leverage = "Leverage"

    AllTags = ",".join((AccountType, NetLiquidation, TotalCashValue,
        SettledCash, AccruedCash, BuyingPower, EquityWithLoanValue,
        PreviousEquityWithLoanValue, GrossPositionValue, ReqTEquity,
        ReqTMargin, SMA, InitMarginReq, MaintMarginReq, AvailableFunds, 
        ExcessLiquidity , Cushion, FullInitMarginReq, FullMaintMarginReq, 
        FullAvailableFunds, FullExcessLiquidity,
        LookAheadNextChange, LookAheadInitMarginReq, LookAheadMaintMarginReq,
        LookAheadAvailableFunds, LookAheadExcessLiquidity, HighestSeverity,
        DayTradesRemaining, Leverage))


