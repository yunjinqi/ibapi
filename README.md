# ibapi

#### 介绍
TWS API官方版本(python版本)的注释和改进版本，目前主要是注释，以便增加对ibapi的了解。

#### 安装教程

```git
##### 如果python包在C:\ProgramData\Anaconda3\Lib\site-packages，则
cd C:\ProgramData\Anaconda3\Lib\site-packages 
git clone https://gitee.com/yunjinqi/ibapi.git
```

#### 使用说明

写了一个付费专栏，分享关于使用TWS的点滴知识与技巧，可以关注：https://blog.csdn.net/qq_26948675/category_11519786.html



